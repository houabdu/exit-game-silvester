import React from "react";
import "./App.css";
import Bonus from "./components/Bonus";
import Finish from "./components/Finish";
import Kapitel1 from "./components/Kapitel1";
import Kapitel12 from "./components/Kapitel1_2";
import Kapitel2 from "./components/Kapitel2";
import Kapitel21 from "./components/Kapitel2_1";
import Kapitel3 from "./components/Kapitel3";
import Kapitel31 from "./components/Kapitel3_1";
import Kapitel4 from "./components/Kapitel4";
import Kapitel41 from "./components/Kapitel4_1";
import Kapitel5 from "./components/Kapitel5";
import Kapitel51 from "./components/Kapitel5_1";
import Kapitel6 from "./components/Kapitel6";
import Kapitel61 from "./components/Kapitel6_1";
import Kapitel7 from "./components/Kapitel7";
import Kapitel71 from "./components/Kapitel7_1";
import Kapitel8 from "./components/Kapitel8";
import Login from "./components/Login";
import useLocalStorage from "./hooks/useLocalStorage";

function App() {
  const [stage, setStage] = useLocalStorage("@stage", "");

  const handleStageChange = (newStage) => {
    setStage(newStage);
  };

  if (stage === "Kapitel1") {
    return <Kapitel1 onChangeStage={handleStageChange} />;
  }

  if (stage === "Kapitel1_2") {
    return <Kapitel12 onChangeStage={handleStageChange} />;
  }

  if (stage === "Kapitel2") {
    return <Kapitel2 onChangeStage={handleStageChange} />;
  }

  if (stage === "Kapitel2_1") {
    return <Kapitel21 onChangeStage={handleStageChange} />;
  }

  if (stage === "Kapitel3") {
    return <Kapitel3 onChangeStage={handleStageChange} />;
  }

  if (stage === "Kapitel3_1") {
    return <Kapitel31 onChangeStage={handleStageChange} />;
  }

  if (stage === "Kapitel4") {
    return <Kapitel4 onChangeStage={handleStageChange} />;
  }

  if (stage === "Kapitel4_1") {
    return <Kapitel41 onChangeStage={handleStageChange} />;
  }

  if (stage === "Kapitel5") {
    return <Kapitel5 onChangeStage={handleStageChange} />;
  }

  if (stage === "Kapitel5_1") {
    return <Kapitel51 onChangeStage={handleStageChange} />;
  }

  if (stage === "Kapitel6") {
    return <Kapitel6 onChangeStage={handleStageChange} />;
  }

  if (stage === "Kapitel6_1") {
    return <Kapitel61 onChangeStage={handleStageChange} />;
  }

  if (stage === "Kapitel7") {
    return <Kapitel7 onChangeStage={handleStageChange} />;
  }

  if (stage === "Kapitel7_1") {
    return <Kapitel71 onChangeStage={handleStageChange} />;
  }

  if (stage === "Kapitel8") {
    return <Kapitel8 onChangeStage={handleStageChange} />;
  }

  if (stage === "Bonusaufgabe") {
    return <Bonus onChangeStage={handleStageChange} />;
  }

  if (stage === "Ende") {
    return <Finish onChangeStage={handleStageChange} />;
  }

  return <Login onChangeStage={handleStageChange} />;
}

export default App;
