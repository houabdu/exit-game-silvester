import React from "react";
import "../App.css";

function Kapitel5({ onChangeStage }) {
  return (
    <div
      className="container"
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        width: "100%",
        minHeight: "100vh",
        backgroundImage: `url("https://scontent-dus1-1.xx.fbcdn.net/v/t31.0-8/11059932_1101545723193921_1246880407324394091_o.jpg?_nc_cat=102&ccb=2&_nc_sid=cdbe9c&_nc_ohc=zTsLPwwehrkAX8GSm1d&_nc_ht=scontent-dus1-1.xx&oh=3cbabfb392dfb063584c76dc67e15c8d&oe=600F0F13")`,
      }}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          width: "60%",
          paddingRight: 31,
          paddingLeft: 31,
          backgroundColor: "lightgrey",
          alignItems: "center",
          justifyContent: "center",
          opacity: 0.8,
          textAlign: "left",
          paddingBottom: 14,
        }}
      >
        <h2>Kapitel 5</h2>
        <p>
          Seufzend rennt ihr zurück zum Orgabüro, zum Glück ist die Tür hier
          wenigstens offen.
          <br />
          <br />
          Drinnen schaut ihr euch um und es sitzt tatsächlich ein junger Mann am
          Tisch, der offenbar zum Orgateam gehört und hektisch am Telefonieren
          ist. Trotzdem guckt er kurz hoch, als er euch herein kommen sieht.
          <br />
          <br />
          „Seid ihr die Truppe, die den Strom wieder herstellen will?“
          <br />
          <br />
          „Insha allah“, nickt ihr. „Aber dazu brauchen wir den Schlüssel von
          Achims Lager, Achim sagt, der liegt hier im Orgabüro. Wir brauchen
          eine neue Hauptsicherung.“
          <br />
          <br />
          Der junge Mann überlegt kurz. Dann nickt er.
          <br />
          <br />
          „Der Schlüssel ist in einer von den Jacken da drüben. Ich weiß aber
          nicht mehr exakt, in welcher, aber ich kann mich an ein paar Details
          erinnern. Sorry, seit 3 Tagen zu wenig Schlaf haben mein Gehirn etwas
          mürbe gemacht.“
          <br />
          <br />
          „Kein Problem“, beruhigt ihr ihn. „Sag uns einfach, was du noch weißt,
          wir finden es schon raus.“
        </p>
        <div>
          <button onClick={() => onChangeStage("Kapitel4_1")}>Zurück</button>
          <button onClick={() => onChangeStage("Kapitel5_1")}>Weiter</button>
        </div>
      </div>
    </div>
  );
}

export default Kapitel5;
