import React from "react";
import "../App.css";

function Finish({ onChangeStage }) {
  return (
    <div
      className="container"
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        width: "100%",
        minHeight: "100vh",
        backgroundImage: `url(${require("../assets/raum.png")})`,
      }}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          width: "60%",
          paddingRight: 31,
          paddingLeft: 31,
          backgroundColor: "lightgrey",
          alignItems: "center",
          justifyContent: "center",
          opacity: 0.9,
          textAlign: "left",
          paddingBottom: 14,
        }}
      >
        <h2>Danke!</h2>
        <p>
          Wir hoffen es hat dir Spaß gemacht! Wir sehen uns bei der
          Siegerehrung!
        </p>
        <iframe
          src="https://giphy.com/embed/9xt1MUZqkneFiWrAAD"
          title="gif"
          width="480"
          height="480"
          frameBorder="0"
          class="giphy-embed"
          allowFullScreen
        ></iframe>
        <button onClick={() => onChangeStage("Bonusaufgabe")}>Zurück</button>
      </div>
    </div>
  );
}

export default Finish;
