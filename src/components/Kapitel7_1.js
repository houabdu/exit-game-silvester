import React, { useState } from "react";
import "../App.css";

function Kapitel7_1({ onChangeStage }) {
  const [jacked, setJacked] = useState();

  const validateResult = () => {
    if (jacked === "WasserFürAlle") {
      onChangeStage("Kapitel8");
    } else {
      alert(
        "Leider ist die Antwort falsch, bei Fragen meld dich bei dem Exit Team"
      );
    }
  };
  return (
    <div
      className="container"
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        width: "100%",
        minHeight: "100vh",
        backgroundImage: `url(${require("../assets/raum.png")})`,
      }}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          width: "60%",
          paddingRight: 31,
          paddingLeft: 31,
          backgroundColor: "lightgrey",
          alignItems: "center",
          justifyContent: "center",
          opacity: 1,
          textAlign: "left",
          paddingBottom: 14,
        }}
      >
        <h2>Kapitel 7</h2>
        <p>
          Die Schwester aus dem Orgateam seufzt. „Ohje, das ist natürlich
          überhaupt nicht gut. Da kann Achim sich bestimmt erst morgen drum
          kümmern. Wie gut, dass ihr wenigstens die Kanister habt.“ Ihr Blick
          fällt bittend auf euch. „Wir wollen in 10 Minuten endlich beten.
          Könntet ihr den Kindern helfen?“
          <br />
          <br />
          Ihr guckt euch an und stimmt zu. Bis jetzt habt ihr alles gemeistert,
          da wird ein bisschen Wasser verteilen ja nicht mehr das Problem sein.
          <br />
          <br />
          Ihr folgt dem Mädchen bis zu dem Weg, der die Häuser trennt, und wo
          Mädchen und Jungen abwartend bei den Kanistern stehen und darüber
          fachsimpeln, wie man das Wasser am besten aufteilt.
          <br />
          <br />
          <img
            alt="no-alt"
            style={{
              height: "30vh",
            }}
            src={require("../assets/Kanister.png")}
          />
          <br />
          <br />
          Vor euch stehen zwei Kanister, einer fasst fünf, der andere drei
          Liter. Wie könnt ihr nun vier Liter abmessen, um das Wasser gerecht
          zwischen Mädchen und Jungen aufzuteilen? Falls ihr Wasser bei Seite
          legen wollt, bevor ihr es weiter abmesst, könnt ihr es in einen
          nebenstehenden Eimer umfüllen, der allerdings nicht zum Abmessen
          genutzt werden kann.
          <br />
          <br />
          Erklärt uns in kurzen Sätzen wie man das Wasser genau abmessen kann.
          <br />
          <br />
          Schicke uns deine Lösung mit deinem Gruppennamen per Mail an:{"  "}
          <a href="mailto: exit2020@muslimischejugend.de">
            exit2020@muslimischejugend.de
          </a>
          <br />
          und du erhälst einen Lösungscode mit dem du weitermachen kannst.
        </p>
        <div>
          <input
            style={{
              width: "200px",
            }}
            type="text"
            value={jacked}
            onChange={(event) => setJacked(event.target.value)}
          />
          <button onClick={validateResult}>Überprüfen</button>
          <button onClick={() => onChangeStage("Kapitel7")}>Zurück</button>
        </div>
      </div>
    </div>
  );
}

export default Kapitel7_1;
