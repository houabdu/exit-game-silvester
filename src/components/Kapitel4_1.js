import React, { useState } from "react";
import "../App.css";

function Kapitel4_1({ onChangeStage }) {
  const [roomname, setRoomName] = useState();

  const validateResult = () => {
    if (roomname.toLowerCase() === "orgabüro") {
      onChangeStage("Kapitel5");
    } else {
      alert(
        "Hier befindet sich leider keine Ersatzsicherung... Probiere einen anderen Raum aus"
      );
    }
  };

  return (
    <div
      className="container"
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        width: "100%",
        minHeight: "100vh",
        backgroundImage: `url("https://scontent-dus1-1.xx.fbcdn.net/v/t31.0-8/11059932_1101545723193921_1246880407324394091_o.jpg?_nc_cat=102&ccb=2&_nc_sid=cdbe9c&_nc_ohc=zTsLPwwehrkAX8GSm1d&_nc_ht=scontent-dus1-1.xx&oh=3cbabfb392dfb063584c76dc67e15c8d&oe=600F0F13")`,
      }}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          width: "60%",
          paddingRight: 31,
          paddingLeft: 31,
          backgroundColor: "lightgrey",
          alignItems: "center",
          justifyContent: "center",
          opacity: 0.8,
          textAlign: "left",
          paddingBottom: 14,
        }}
      >
        <h2>Kapitel 4</h2>
        <img
          alt="no-alt"
          style={{
            height: "50vh",
          }}
          src={require("../assets/AchimsZettel.jpg")}
        />
        <p>
          Etwas ratlos seht ihr euch an. Die Telefonnummer macht gar keinen
          Sinn. Achim, der Spaßvogel, will immer alle zum Denken anregen, das
          wisst ihr. Aber dass er dann gleich seine Telefonnummer kodiert…
          <br />
          <br />
          „Jammern bringt nichts, wir brauchen Strom, damit wir beten können“,
          sagt eine Person aus eurer Gruppe resolut und ihr nickt entschieden.{" "}
          <br />
          <br />
          Achim hat sicherlich irgendwo einen Hinweis hinterlassen, wie man
          seine Nummer entschlüsseln kann. Ihr guckt euch die Infos an der Tür
          genau an und habt plötzlich einen Geistesblitz.
          <br />
          <br />
          Wohin müsst ihr als nächstes? Tragt nur das Wort ein.
        </p>
        <div>
          <input
            style={{
              width: "200px",
            }}
            type="text"
            value={roomname}
            onChange={(event) => setRoomName(event.target.value)}
          />
          <button onClick={validateResult}>Überprüfen</button>
          <button onClick={() => onChangeStage("Kapitel4")}>Zurück</button>
        </div>
      </div>
    </div>
  );
}

export default Kapitel4_1;
