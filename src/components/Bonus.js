import React from "react";
import "../App.css";

function Bonus({ onChangeStage }) {
  return (
    <div
      className="container"
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        width: "100%",
        minHeight: "100vh",
        backgroundImage: `url(${require("../assets/raum.png")})`,
      }}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          width: "60%",
          paddingRight: 31,
          paddingLeft: 31,
          backgroundColor: "lightgrey",
          alignItems: "center",
          justifyContent: "center",
          opacity: 0.9,
          textAlign: "left",
          paddingBottom: 14,
        }}
      >
        <h2>Bonusaufgabe</h2>
        <p>
          Herzlichen Glückwunsch, ihr habt die Rätsel gelöst und das Gebet auf
          dem Meeting gerettet! Wenn ihr noch Zeit übrig habt, haben wir noch
          ein Bonusrätsel für euch, das euch Extrapunkte verschaffen wird.
          <br />
          <br />
          Surenrätsel: Welche beiden Suren wird der Imam rezitieren? Schickt uns
          die Lösung über das Kontaktformular!
          <br />
          <br />
          Hinweise:
          <br />
          <br />
          1. Beide Suren sind aus dem 29. oder 30. Juz <br />
          2. Beide Suren haben jeweils weniger als 50 Verse
          <br />
          3. Es sind rein mekkanische Suren
          <br />
          4. Namen der Suren sind keine Tiere oder andere Geschöpfe <br />
          5. Keine der Suren beginnt mit einem Schwur („wa“)
          <br />
          6. Die Nummer der Suren sind zweistellig
          <br />
          7. Es ist nicht die Sure, welche Verse als erstes offenbart wurden
          <br />
          8. Die Suren beginnen weder mit "idha" noch mit "la"
          <br />
          9. Die Nummer der Suren sind nicht durch 10 teilbar (z.B. 40 oder 50)
          <br />
          10. Zwischen beiden Suren ist ein Abstand von 10 Suren (z.B. 22 und
          32)
          <br />
          11. Keine der Suren ist eine "Schnapszahl" (z.B. 33 oder 44)
          <br />
          12. Quersumme der Suren darf nicht 10 ergeben.
          <br />
          <br />
          <br />
          Schicke uns deine Lösung mit deinem Gruppennamen per Mail an:{"  "}
          <a href="mailto: exit2020@muslimischejugend.de">
            exit2020@muslimischejugend.de
          </a>
          <br />
          <br />
          Ihr seid fertig! Danke fürs teilnehemen!
        </p>
        <div>
          <button onClick={() => onChangeStage("Kapitel8")}>Zurück</button>
          <button onClick={() => onChangeStage("Ende")}>Weiter</button>
        </div>
      </div>
    </div>
  );
}

export default Bonus;
