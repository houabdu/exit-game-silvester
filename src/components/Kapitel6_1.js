import React, { useState } from "react";
import "../App.css";

function Kapitel6_1({ onChangeStage }) {
  const [jacked, setJacked] = useState();

  const validateResult = () => {
    if (jacked === "BoxBoxBox") {
      onChangeStage("Kapitel7");
    } else {
      alert(
        "Leider ist die Antwort falsch, bei Fragen meld dich bei dem Exit Team"
      );
    }
  };

  return (
    <div
      className="container"
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        width: "100%",
        minHeight: "100vh",
        backgroundImage: `url("https://scontent-dus1-1.xx.fbcdn.net/v/t31.0-8/11059932_1101545723193921_1246880407324394091_o.jpg?_nc_cat=102&ccb=2&_nc_sid=cdbe9c&_nc_ohc=zTsLPwwehrkAX8GSm1d&_nc_ht=scontent-dus1-1.xx&oh=3cbabfb392dfb063584c76dc67e15c8d&oe=600F0F13")`,
      }}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          width: "60%",
          paddingRight: 31,
          paddingLeft: 31,
          backgroundColor: "lightgrey",
          alignItems: "center",
          justifyContent: "center",
          opacity: 0.8,
          textAlign: "left",
          paddingBottom: 14,
        }}
      >
        <h2>Kapitel 6</h2>
        <img
          alt="no-alt"
          style={{
            height: "30vh",
          }}
          src={require("../assets/Boxen.png")}
        />
        <p>
          Erklärt uns in kurzen Sätzen wie ihr mit zwei Mal wiegen die richtige
          Box findet.
          <br />
          <br />
          Schicke uns deine Lösung mit deinem Gruppennamen per Mail an:{" "}
          <a href="mailto: exit2020@muslimischejugend.de">
            exit2020@muslimischejugend.de
          </a>
          <br />
          und du erhälst einen Lösungscode mit dem du weitermachen kannst.
        </p>
        <div>
          <input
            style={{
              width: "200px",
            }}
            type="text"
            value={jacked}
            onChange={(event) => setJacked(event.target.value)}
          />
          <button onClick={validateResult}>Überprüfen</button>
          <button onClick={() => onChangeStage("Kapitel6")}>Zurück</button>
        </div>
      </div>
    </div>
  );
}

export default Kapitel6_1;
