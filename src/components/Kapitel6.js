import React from "react";
import "../App.css";

function Kapitel6({ onChangeStage }) {
  return (
    <div
      className="container"
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        width: "100%",
        minHeight: "100vh",
        backgroundImage: `url(${require("../assets/raum.png")})`,
      }}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          width: "60%",
          paddingRight: 31,
          paddingLeft: 31,
          backgroundColor: "lightgrey",
          alignItems: "center",
          justifyContent: "center",
          opacity: 0.9,
          textAlign: "left",
          paddingBottom: 14,
        }}
      >
        <h2>Kapitel 6</h2>
        <p>
          Mit dem Schlüssel in der Hand eilt ihr zum Lager, die Lösung des
          Problems ist endlich zum Greifen nah. Ohne Probleme passt der
          Schlüssel ins Schloss und ihr tretet ins Lager hinein.
          <br />
          <br />
          Drinnen herrscht absolutes Chaos.
          <br />
          <br />
          Ihr leuchtet umher, um die Ersatzteile für die Elektronik zu finden
          und schließlich sticht euch ein Stapel gleichgroßer Kartons ins Auge.
          Sie sind allesamt verschlossen und zugeklebt, aber auf einem klebt ein
          Inventarzettel.
          <br />
          <br />
          „In einer dieser Boxen ist eine Hauptsicherung versteckt. In den
          anderen liegen Gegenstände, die fast genauso schwer sind, aber alle
          etwas leichter und gleich schwer. Wer herausbekommen will, wo die
          Hauptsicherung drin ist, muss die Waage benutzen. Doch Vorsicht, die
          Waage ist schon sehr alt und wird nur noch zwei Mal benutzt werden
          können. Auf jeder Seite dürfen beliebig viele Boxen liegen. Wie
          schafft ihr es mit zwei Mal wiegen die richtige Box zu finden?
          Natürlich dürft ihr nur eine einzige Box aufmachen. Euer Achim“
          <br />
          <br />
        </p>
        <div>
          <button onClick={() => onChangeStage("Kapitel5_1")}>Zurück</button>
          <button onClick={() => onChangeStage("Kapitel6_1")}>Weiter</button>
        </div>
      </div>
    </div>
  );
}

export default Kapitel6;
