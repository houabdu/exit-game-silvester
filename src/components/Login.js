import React, { useState } from "react";
import "../App.css";

function Login({ onChangeStage }) {
  const [password, setPassword] = useState();

  const handleChange = (event) => {
    setPassword(event.target.value);
  };

  const handleSubmit = (event) => {
    if (password === "ichwillhierraus") onChangeStage("Kapitel1");
    else {
      alert("Passwort falsch");
    }
  };

  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column",
        width: "100%",
        height: "100vh",
        backgroundImage: `url("https://images.unsplash.com/photo-1520033906782-1684d0e7498e?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=1867&q=80")`,
      }}
    >
      <p style={{
          alignItems: 'center',
          justifyContent: 'center',
          width: "60%", 
          textAlign: "center"
      }}>
        <h1>Rettet das Gebet!</h1>
        <h2>
          Willkommen beim Exit Game der MJD in der Silvesternacht
        </h2>
      </p>
      <h2>Bitte gib deinen Zugangscode ein</h2>
      <p>
        <form onSubmit={handleSubmit}>
          <label>
            <input
              type="text"
              name="name"
              value={password}
              onChange={handleChange}
            />
          </label>
          <input type="submit" value="starten" />
        </form>
      </p>
      <br />
    </div>
  );
}

export default Login;
