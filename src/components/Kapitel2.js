import React from "react";
import "../App.css";

function Kapitel2({ onChangeStage }) {
  return (
    <div
      className="container"
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        width: "100%",
        minHeight: "100vh",
        backgroundImage: `url("https://scontent-dus1-1.xx.fbcdn.net/v/t31.0-8/11059932_1101545723193921_1246880407324394091_o.jpg?_nc_cat=102&ccb=2&_nc_sid=cdbe9c&_nc_ohc=zTsLPwwehrkAX8GSm1d&_nc_ht=scontent-dus1-1.xx&oh=3cbabfb392dfb063584c76dc67e15c8d&oe=600F0F13")`,
      }}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          width: "60%",
          paddingRight: 31,
          paddingLeft: 31,
          backgroundColor: "lightgrey",
          alignItems: "center",
          justifyContent: "center",
          opacity: 0.8,
          textAlign: "left",
          paddingBottom: 14,
        }}
      >
        <h2>Kapitel 2</h2>
        <p>
          Einigermaßen ratlos steht ihr vor dem Sicherungskasten. Natürlich, das
          war klar… ein Sicherungskasten, der in einer Jugendherberge hängt,
          wird sicherlich nicht einfach so zu öffnen sein… aber dass ihr erstmal
          rätseln müsst, bevor ihr an den Pin kommt, hättet ihr nicht gedacht.
          Das Zahlenschloss verlangt nach 3 Ziffern – aber woher sollt ihr die
          bekommen? <br />
          <br />
          Etwas ratlos sucht ihr die Umgebung ab, bis ihr plötzlich beim
          Absuchen des Sicherungskastens hinter dem Kasten eingeklemmt zwischen
          Kasten und Hauswand einen zerknitterten Zettel findet. Offenbar hat
          der Herbergsvater nicht mehr so das beste Gedächtnis und hat sich
          deswegen eine Gedächtnisstütze gemacht. <br />
          <br />
          Also steckt ihr kurzerhand die Köpfe zusammen und versucht, das Rätsel
          zu lösen.
        </p>
        <div>
          <button onClick={() => onChangeStage("Kapitel1_2")}>Zurück</button>
          <button onClick={() => onChangeStage("Kapitel2_1")}>Weiter</button>
        </div>
      </div>
    </div>
  );
}

export default Kapitel2;
