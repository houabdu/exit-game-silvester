import React from "react";
import bgImage from "../assets/meeting_prayer.jpg";
import "../App.css";

function Kapitel1_2({ onChangeStage }) {
  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        width: "100%",
        minHeight: "100vh",
        backgroundImage: `url(${bgImage})`,
      }}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          width: "60%",
          paddingRight: 31,
          paddingLeft: 31,
          backgroundColor: "lightgrey",
          alignItems: "center",
          justifyContent: "center",
          opacity: 0.8,
          textAlign: "left",
          paddingBottom: 14,
        }}
      >
        <h2>Kapitel 1</h2>
        <p>
          mit einem gewaltigen Knall alle Lichter ausgehen. Im Chor geht ein
          gewaltiger Aufschrei durch die Menge, als plötzlich fast 1000 Menschen
          im Dunkeln sitzen. <br />
          <br /> Habt ihr vorher gedacht, es wäre laut? Jetzt legt die
          Lautstärke erst richtig zu. Alle fachsimpeln und diskutieren, manche
          geraten offenbar beinahe in Panik, an jeder Ecke werden
          Handytaschenlampen gezückt und die ersten Leute versuchen, sich total
          witzig findend, andere im Dunkeln zu erschrecken. Ihr könnt darüber
          nur die Köpfe schütteln, während ihr euch im Licht eurer Handylampen
          suchend umschaut. <br />
          <br /> Da, am Zelteingang erscheint ein Mädchen mit Orgateam-Weste.
          Sie hat eine Taschenlampe in der Hand und winkt euch zu. <br /> Zuerst
          seid ihr verwirrt. Aber sie meint offenbar euch, denn sie winkt immer
          energischer. Schulterzuckend steht ihr also auf und geht zu ihr. „Ich
          hab euch schon gesucht. Offenbar gab es einen Stromausfall. Wir vom
          Orgateam versuchen jetzt, die Leute zu organisieren, und ich weiß,
          dass ihr technisch begabt seid. Könntet ihr vielleicht nach dem
          Sicherungskasten gucken und dafür sorgen, dass wir wieder Strom
          bekommen?“ <br />
          <br /> Ihr habt keine Ahnung, woher sie von eurem Technikinteresse
          weiß, aber ihr fühlt euch auf jeden Fall geehrt und seid gerne bereit
          zu helfen. <br />
          „Der Sicherungskasten ist links am Haus, wo das Orgabüro ist“, ruft
          sie euch noch zu, bevor sie in der aufgeregten Masse verschwindet.{" "}
          <br />
          Schon ein bisschen aufgeregt über eure wichtige Aufgabe macht ihr euch
          auf zum Sicherungskasten.
        </p>
        <div>
          <button onClick={() => onChangeStage("Kapitel1")}>Zurück</button>
          <button onClick={() => onChangeStage("Kapitel2")}>Weiter</button>
        </div>
      </div>
    </div>
  );
}

export default Kapitel1_2;
