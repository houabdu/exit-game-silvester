import React from "react";
import "../App.css";

function Kapitel7({ onChangeStage }) {
  return (
    <div
      className="container"
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        width: "100%",
        minHeight: "100vh",
        backgroundImage: `url(${require("../assets/raum.png")})`,
      }}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          width: "60%",
          paddingRight: 31,
          paddingLeft: 31,
          backgroundColor: "lightgrey",
          alignItems: "center",
          justifyContent: "center",
          opacity: 0.9,
          textAlign: "left",
          paddingBottom: 14,
        }}
      >
        <h2>Kapitel 7</h2>
        <p>
          Das war ein Akt. Geistig mittlerweile schon ziemlich erschöpft trabt
          ihr zurück zum Sicherungskasten, die Ersatzhauptsicherung stolz vor
          euch her tragend. Wie einen Schatz setzt ihr sie in den
          Sicherungskasten ein, legt den Sicherungsschalter um und von allen
          Seiten ertönen überraschte Rufe im Feriendorf, als überall das Licht
          wieder angeht. Jubelnd klopft ihr euch gegenseitig auf die Schultern,
          vergesst in eurem Freudentaumel nicht, den Sicherungskasten wieder
          sachgemäß zu verschließen und lauft dann zurück zum Gebetszelt.
          <br />
          <br />
          Es ist schon einige Zeit vergangen, seit dem ihr auf die Suche nach
          der Lösung gegangen seid und viele Leute sind schon aus dem Zelt
          verschwunden.
          <br />
          <br />
          „Das Gebet wird in 10 Minuten insha Allah im Gebetszelt stattfinden“,
          ertönt die Ansage durch die Lautsprecher auf dem Gelände und ihr
          blickt euch zufrieden an.
          <br />
          <br />
          Plötzlich kommt das Mädchen vom Orgateam wieder auf euch und strahlt
          euch an.
          <br />
          <br />
          „Barak allahu feekum, das war hervorragende Arbeit! Ich wusste, ihr
          kriegt das hin.“
          <br />
          <br />
          Als ihr ihr Lob gerade bescheiden abwehren wollt, kommt ein jüngeres
          Mädchen zu ihr und tippt sie an.
          <br />
          <br />
          „Du, wir wollen noch mal Wudu machen, aber irgendwie funktioniert das
          Wasser in unserem Haus nicht. Das Jungenhaus auf der anderen Seite hat
          offenbar dasselbe Problem. Wir haben nur zwei Kanister und wir wollen
          es gerecht unter uns aufteilen, aber wir wissen nicht wie.“
        </p>
        <div>
          <button onClick={() => onChangeStage("Kapitel6_1")}>Zurück</button>
          <button onClick={() => onChangeStage("Kapitel7_1")}>Weiter</button>
        </div>
      </div>
    </div>
  );
}

export default Kapitel7;
