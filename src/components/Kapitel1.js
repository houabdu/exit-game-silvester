import React from "react";
import bgImage from '../assets/meeting_prayer.jpg'
import "../App.css";

function Kapitel1({ onChangeStage }) {
  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        width: "100%",
        minHeight: "100vh",
        backgroundImage: `url(${bgImage})`,
      }}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          width: "60%",
          paddingRight: 31,
          paddingLeft: 31,
          backgroundColor: "lightgrey",
          alignItems: "center",
          justifyContent: "center",
          opacity: 0.8,
          textAlign: "left",
          paddingBottom: 14,
        }}
      >
        <h2>Kapitel 1</h2>
        <p>
          Das Jahresmeeting der MJD ist in vollem Gange und du bist mit deiner
          Freundesgruppe motiviert dabei. Gemeinsam genießt ihr die Vorträge,
          werdet in den AGs aktiv und gönnt euch ordentlich an den zahlreichen
          Essensständen, deren Duft von Pommes, Falafel, Waffeln und Co selbst
          Nachts über dem Gelände hängt. <br />
          <br /> Außerdem spielt das Wetter ganz hervorragend mit. Es ist warm
          und sonnig, aber nie so heiß, dass die zahlreichen Außenaktivitäten
          darunter leiden könnten. Nachts leuchten auf dem ganzen Gelände
          Lichterketten und Laternen, so wie auch an diesem Abend. <br />
          <br /> Ihr habt euch gerade für das Abendgebet fertig gemacht und
          betretet gemeinsam das rappelvolle Gebetszelt, das ebenfalls von
          vielen Laternen und Lampions in gemütliches Licht getaucht wird. Es
          herrscht ohrenbetäubender Lärm, weil alle gleichzeitig reden und sich
          von ihrem Tag berichten, in jeder Ecke wird gelacht und erzählt.{" "}
          <br />
          <br />
          Ihr steht dem in nichts nach, bis plötzlich
          <br />
          <br />
        </p>
          <button onClick={() => onChangeStage("Kapitel1_2")}>Weiter</button>
      </div>
    </div>
  );
}

export default Kapitel1;
