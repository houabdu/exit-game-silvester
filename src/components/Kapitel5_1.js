import React, { useState } from "react";
import "../App.css";

function Kapitel5_1({ onChangeStage }) {
  const [jacked, setJacked] = useState();

  const validateResult = () => {
    if (jacked === "7") {
      onChangeStage("Kapitel6");
    } else {
      alert(
        "Hier befindet sich leider keine Schlüssel... Schaue in einer anderen Jacke nach"
      );
    }
  };

  return (
    <div
      className="container"
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        width: "100%",
        minHeight: "100vh",
        backgroundImage: `url("https://scontent-dus1-1.xx.fbcdn.net/v/t31.0-8/11059932_1101545723193921_1246880407324394091_o.jpg?_nc_cat=102&ccb=2&_nc_sid=cdbe9c&_nc_ohc=zTsLPwwehrkAX8GSm1d&_nc_ht=scontent-dus1-1.xx&oh=3cbabfb392dfb063584c76dc67e15c8d&oe=600F0F13")`,
      }}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          width: "60%",
          paddingRight: 31,
          paddingLeft: 31,
          backgroundColor: "lightgrey",
          alignItems: "center",
          justifyContent: "center",
          opacity: 0.8,
          textAlign: "left",
          paddingBottom: 14,
        }}
      >
        <h2>Kapitel 5</h2>
        <p>
          Er nickt, schließt kurz die Augen, um sich zu sammeln und sagt dann:
          <br />
          <br />
          „Da vorne hängen 10 Jacken mit unterschiedlichen Farben nebeneinander
          auf den Ständer. Wir haben die Position der Jacken seit heute Morgen
          nicht verändert, es war ja so warm. Jeder Haken ist belegt. Am ersten
          Haken hängt eine rote Jacke. Direkt daneben hängt weder eine schwarze,
          noch eine weiße Jacke. Zwischen der braunen und pinkfarbenen Jacke
          hängen zwei Jacken. Der Abstand zwischen der weißen und der schwarzen
          Jacke war größtmöglich. Zwischen der blauen und der grünen Jacke
          hängen die weiße, die gelbe und eine weitere Jacke. Rechts neben der
          blauen Jacke hängen vier Jacken. Links neben der pinkfarbenen Jacke
          hängen sieben Jacken. Zwischen der Jacke, in der der Schlüssel ist,
          und der grauen Jacke hängt nur eine Jacke. Die gelbe Jacke hängt näher
          an der Jacke, die ihr sucht, als an der pinkfarbenen Jacke.“
          <br />
          <br />
          In der wievielten Jacke ist der Schlüssel? Tragt nur die Nummer als
          Zahl ein.
        </p>
        <div>
          <input
            style={{
              width: "200px",
            }}
            type="text"
            value={jacked}
            onChange={(event) => setJacked(event.target.value)}
          />
          <button onClick={validateResult}>Überprüfen</button>
          <button onClick={() => onChangeStage("Kapitel5")}>Zurück</button>
        </div>
      </div>
    </div>
  );
}

export default Kapitel5_1;
