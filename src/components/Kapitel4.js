import React from "react";
import "../App.css";

function Kapitel4({ onChangeStage }) {
  return (
    <div
      className="container"
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        width: "100%",
        minHeight: "100vh",
        backgroundImage: `url("https://scontent-dus1-1.xx.fbcdn.net/v/t31.0-8/11059932_1101545723193921_1246880407324394091_o.jpg?_nc_cat=102&ccb=2&_nc_sid=cdbe9c&_nc_ohc=zTsLPwwehrkAX8GSm1d&_nc_ht=scontent-dus1-1.xx&oh=3cbabfb392dfb063584c76dc67e15c8d&oe=600F0F13")`,
      }}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          width: "60%",
          paddingRight: 31,
          paddingLeft: 31,
          backgroundColor: "lightgrey",
          alignItems: "center",
          justifyContent: "center",
          opacity: 0.8,
          textAlign: "left",
          paddingBottom: 14,
        }}
      >
        <h2>Kapitel 4</h2>
        <p>
          Zufrieden betrachtet ihr euer Werk, nachdem ihr alle Sicherungsstecker
          wieder richtig angebracht habt. Nur – die Hauptsicherung ist kaputt,
          wenn man sie genau anguckt, qualmt sie sogar noch ein bisschen, also
          entfernt ihr sie lieber. Es muss eine neue Sicherung her… nur… woher?
          <br />
          <br />
          „Achi!“
          <br />
          <br />
          Alle fahren hoch. Natürlich. Achim Wieselsberg, der Herbergsvater,
          wird von euch allen nur liebevoll und mit etwas Ironie „Achi“ genannt
          und ist die gute Seele der Herberge. Und außerdem ist er ein
          Herbergsvater vom mindestens genauso alten Schlag wie sein
          Sicherungskasten, denn er repariert am liebsten alles selbst. <br />
          <br />
          Er hat mit Sicherheit eine Ersatzsicherung. Es ist bloß fraglich, ob
          er vor Ort ist, denn mittlerweile ist es schon nach 22 Uhr und der
          arme Mann hat bestimmt auch zwischendurch mal irgendwann Feierabend.
          Aber eine andere Möglichkeit habt ihr nicht und nichts liegt euch
          ferner, als das Vertrauen des Orgateams in euch zu zerstören, in dem
          ihr einfach aufgebt, ohne es zumindest versucht zu haben.
          <br />
          <br />
          Also trabt ihr rasch durch die Dunkelheit zum Haus des Herbergsvaters.
          Alle Fenster sind dunkel und als ihr näher kommt, seht ihr einen
          Zettel an die Haustür gepinnt sanft im Wind wehen. Ihr leuchtet
          darauf, um ihn besser lesen zu können.
        </p>
        <div>
          <button onClick={() => onChangeStage("Kapitel3_1")}>Zurück</button>
          <button onClick={() => onChangeStage("Kapitel4_1")}>Weiter</button>
        </div>
      </div>
    </div>
  );
}

export default Kapitel4;
