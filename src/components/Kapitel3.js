import React from "react";
import "../App.css";

function Kapitel3({ onChangeStage }) {
  return (
    <div
      className="container"
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        width: "100%",
        minHeight: "100vh",
        backgroundImage: `url("https://scontent-dus1-1.xx.fbcdn.net/v/t31.0-8/11059932_1101545723193921_1246880407324394091_o.jpg?_nc_cat=102&ccb=2&_nc_sid=cdbe9c&_nc_ohc=zTsLPwwehrkAX8GSm1d&_nc_ht=scontent-dus1-1.xx&oh=3cbabfb392dfb063584c76dc67e15c8d&oe=600F0F13")`,
      }}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          width: "60%",
          paddingRight: 31,
          paddingLeft: 31,
          backgroundColor: "lightgrey",
          alignItems: "center",
          justifyContent: "center",
          opacity: 0.8,
          textAlign: "left",
          paddingBottom: 14,
        }}
      >
        <h2>Kapitel 3</h2>
        <p>
          Lachend stoßt ihr euch gegenseitig an. Wahnsinn, wie schnell ihr das
          rausbekommen habt. Offenbar liegt euch nicht nur Technik. Klar, man
          muss immer nur die Anzahl der Buchstaben von den Wörtern nehmen und
          schon kommt man auf die Pinnummer. Und mit einem „Klack“ lässt sich
          der Sicherungskasten auch schon öffnen. <br />
          <br />
          Neugierig leuchtet ihr mit euren Lampen hinein und schon offenbart
          sich euch das nächste Problem. Offenbar ist nicht nur die
          Hauptsicherung durchgebrannt, sondern auch sämtliche Sicherungen des
          sehr altertümlichen Sicherungskastens abgesprungen – es herrscht ein
          heilloses Durcheinander. Da bleibt nur eine Möglichkeit, um wieder
          Ordnung reinzubringen: Anfangen zu sortieren. <br />
          <br />
          Euch gegenseitig leuchtend fangt ihr motiviert an, den Kabelsalat zu
          sortieren. Welcher Stecker gehört an welche Sicherung (Sortiert eure
          Antworten aufsteigend anhand der Nummern)?
        </p>
        <div>
          <button onClick={() => onChangeStage("Kapitel2_1")}>Zurück</button>
          <button onClick={() => onChangeStage("Kapitel3_1")}>Weiter</button>
        </div>
      </div>
    </div>
  );
}

export default Kapitel3;
