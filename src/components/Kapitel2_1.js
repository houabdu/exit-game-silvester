import React, { useState } from "react";
import "../App.css";

function Kapitel2_1({ onChangeStage }) {
  const [pin, setPin] = useState();

  const handleChange = (event) => {
    setPin(event.target.value);
  };

  const validatePin = (event) => {
    if (pin === "468") onChangeStage("Kapitel3");
    else {
      alert("Der eingegebene PIN ist leider falsch");
    }
  };

  return (
    <div
      className="container"
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        width: "100%",
        minHeight: "100vh",
        backgroundImage: `url("https://images.unsplash.com/photo-1580064257720-a8f9dc76f773?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1950&q=80")`,
      }}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          width: "60%",
          paddingRight: 31,
          paddingLeft: 31,
          backgroundColor: "lightgrey",
          alignItems: "center",
          justifyContent: "center",
          opacity: 0.9,
          textAlign: "left",
          paddingBottom: 14,
        }}
      >
        <h2>Kapitel 2</h2>
        <div>
          <img
            alt="no-alt"
            style={{
              height: "70vh",
            }}
            src={require("../assets/Schloss.png")}
          />
          <div
            style={{
              display: "flex",
              justifyContent: "center",
            }}
          >
            <input
              style={{
                flex: 1,
                display: "flex",
              }}
              type="text"
              name="pin"
              value={pin}
              onChange={handleChange}
            />
            <button onClick={validatePin}>überprüfen</button>
            <button onClick={() => onChangeStage("Kapitel2")}>Zurück</button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Kapitel2_1;
