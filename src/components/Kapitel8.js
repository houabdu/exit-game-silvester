import React from "react";
import "../App.css";

function Kapitel8({ onChangeStage }) {
  return (
    <div
      className="container"
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        width: "100%",
        minHeight: "100vh",
        backgroundImage: `url(${require("../assets/raum.png")})`,
      }}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          width: "60%",
          paddingRight: 31,
          paddingLeft: 31,
          backgroundColor: "lightgrey",
          alignItems: "center",
          justifyContent: "center",
          opacity: 0.9,
          textAlign: "center",
          paddingBottom: 14,
        }}
      >
        <h2>Kapitel 8</h2>
        <p>
          Völlig erschöpft, aber sehr zufrieden mit euch wartet ihr auf dem Weg,
          bis alle Kinder Wudu gemacht haben und geht dann gemeinsam mit ihnen
          zum Zelt zurück.
          <br />
          <br />
          Endlich steht ihr alle gemeinsam aufgereiht, im sanften Licht der
          Laternen und Lichterketten, und der Imam beginnt mit kräftiger Stimme
          seine Rezitation.
          <br />
          <br />
          <img
            alt="no-alt"
            style={{
              height: "30vh",
            }}
            src={require("../assets/fi.png")}
          />
        </p>
        <div>
          <button onClick={() => onChangeStage("Kapitel7_1")}>Zurück</button>
          <button onClick={() => onChangeStage("Bonusaufgabe")}>Weiter</button>
        </div>
      </div>
    </div>
  );
}

export default Kapitel8;
