import React, { useState } from "react";
import "../App.css";

function Kapitel3_1({ onChangeStage }) {
  const [result1, setResult1] = useState();
  const [result2, setResult2] = useState();
  const [result3, setResult3] = useState();
  const [result4, setResult4] = useState();
  const [result5, setResult5] = useState();

  const validateResult = () => {
    if (
      result1.toLowerCase() === "a" &&
      result2.toLowerCase() === "e" &&
      result3.toLowerCase() === "c" &&
      result4.toLowerCase() === "b" &&
      result5.toLowerCase() === "d"
    ) {
      onChangeStage("Kapitel4");
    } else {
      alert(
        "Kurzschluss! Leider war deine Verkablung fehlerhaft. Probiere es erneut aus!"
      );
    }
  };

  return (
    <div
      className="container"
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        width: "100%",
        minHeight: "100vh",
        backgroundImage: `url("https://images.unsplash.com/photo-1580064257720-a8f9dc76f773?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1950&q=80")`,
      }}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          width: "60%",
          paddingRight: 31,
          paddingLeft: 31,
          backgroundColor: "lightgrey",
          alignItems: "center",
          justifyContent: "center",
          textAlign: "left",
          paddingBottom: 14,
        }}
      >
        <h2>Kapitel 3</h2>
        <div
          style={{
            display: "flex",
            flex: 1,
            flexDirection: "column",
          }}
        >
          <img
            alt="no-alt"
            style={{
              height: "50vh",
            }}
            src={require("../assets/Kabelsalat.jpg")}
          />
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              marginTop: 10,
              marginLeft: 10,
              marginRight: 10,
            }}
          >
            1
            <input
              style={{
                width: "20px",
              }}
              type="text"
              value={result1}
              onChange={(event) => setResult1(event.target.value)}
            />
            2
            <input
              style={{
                width: "20px",
              }}
              type="text"
              value={result2}
              onChange={(event) => setResult2(event.target.value)}
            />
            3
            <input
              style={{
                width: "20px",
              }}
              type="text"
              value={result3}
              onChange={(event) => setResult3(event.target.value)}
            />
            4
            <input
              style={{
                width: "20px",
              }}
              type="text"
              value={result4}
              onChange={(event) => setResult4(event.target.value)}
            />
            5
            <input
              style={{
                width: "20px",
              }}
              type="text"
              value={result5}
              onChange={(event) => setResult5(event.target.value)}
            />
            <button onClick={validateResult}>überprüfen</button>
            <button onClick={() => onChangeStage("Kapitel3")}>Zurück</button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Kapitel3_1;
